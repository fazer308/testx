<!doctype html>

<html lang="pl">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Wyniki głosowania live dla strony podlaska marka</title>

    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/style.css">
    <!--[if lt IE 9]>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.js"></script>
    <![endif]-->
    <script src="./js/Chart.min.js"></script>
    <script>
        var rok = 2018;
        var mies = "Jan";
        var dzien = 30;
        var godziny = 12;
        var minuty = 12;
        var sekundy = 0;
        //var data_przyszla = new Date(rok, mies-1, dzien);
    </script>
</head>

<body>

    
    <div class="container-fluid back-grey spacing_top_small spacing_bottom_small">
        <div class="container">
        <div class="row">

                <div class="col-md-5 text-left">
                    <img src="img/top_zubr.jpg">
                </div>
                <div class="col-md-7 text-right">
                    <div class="row">
                        <div class="col-12 col-sm-12 col-md-7 col-lg-7 col-xl-7">
                        
                        </div>
                        <div class="col-1 offset-6 text-center">
                            <img src="img/top_lupa.jpg">
                        </div>    
                        <div class="col-1 text-center">
                            <img src="img/top_kontrast.jpg">
                        </div>
                       <div class="col-1 text-center">
                            <img src="img/top_a_small.jpg">
                        </div>
                       <div class="col-1 text-center">
                            <img src="img/top_a_medium.jpg">
                        </div>
                       <div class="col-1 text-center">
                            <img src="img/top_a_big.jpg">
                        </div>                    
                    </div>
                    
                    <div class="row">
   
                        <div class="col-12 col-sm col-md col-lg col-xl text-center spacing_top_medium spacing_bottom_medium">
                            <a class="menu blue" href="#">o nagrodzie</a>
                        </div>    
                        <div class="col-12 col-sm col-md col-lg col-xl text-center spacing_top_medium spacing_bottom_medium">
                            <a class="menu blue" href="#">laureaci</a>
                        </div>
                       <div class="col-12 col-sm col-md col-lg col-xl text-center spacing_top_medium spacing_bottom_medium">
                            <a class="menu blue" href="#">ambasadorowie</a>
                        </div>
                       <div class="col-12 col-sm col-md col-lg col-xl text-center spacing_top_medium spacing_bottom_medium">
                            <a class="menu blue" href="#">dla mediów</a>
                        </div>
                       <div class="col-12 col-sm col-md col-lg col-xl text-center spacing_top_medium spacing_bottom_medium">
                            <a class="menu blue" href="#">kontakt</a>
                        </div>                    
                    </div>                    
                </div>
        </div>    
        </div>
     </div>
    
    <div class="background">
        <div class="container">

            <div class="row">
                <div class="col-md-12 text-center blue"><h1 class="spacing_top_big">Podlaska Marka Konsumentów</h1></div>
            </div>

            <div class="row">
                <div class="col-md-12 text-center grey1"><h2 class="spacing_bottom_medium">Poniżej wyniki głosowania w czasie rzeczywistym</h2></div>
            </div>

            <div class="row">
                <div class="col-md-3 text-center">
                    <div class="col-md-12 button_blue_small">Wyniki</div>
                    <div class="col-md-12"></div>
                </div>
                <div class="col-md-3 text-center grey1">
                     <div class="col-md-12">
                        <img src="img/top_man.jpg" />
                    </div>
                    <div class="col-md-12">
                        <h2>2900</h2>
                        <h2 class="arial">Łączna liczba głosów</h2>
                    </div>           
                </div>
                <div class="col-md-3 text-center grey1">
                     <div class="col-md-12">
                        <img src="img/top_monitor.jpg" />
                    </div>
                    <div class="col-md-12">
                        <h2>1400</h2>
                        <h2 class="arial">Liczba głosów <br /> oddanych na lidera</h2>
                    </div>             
                </div>
                <div class="col-md-3 text-center grey1">
                      <div class="col-md-12">
                        <img src="img/top_light.jpg" />
                    </div>
                    <div class="col-md-12">
                        <h2>Firma 1</h2>
                        <h2 class="arial">Aktualny lider</h2>
                    </div>            
                </div>            
            </div>

            <div class="row">
                <div class="col-md-12 spacing_top_medium spacing_bottom_medium"></div>
            </div>

            
            
            <div class="row">
                <div class="col-md-12 button_blue_przed">Czas pozostały  <br /> do końca głosowania</div>
                <div class="col-md-6 text-center spacing_bottom_clock spacing_top_medium red">
                    <div class="row">

                        <div class="col-md-12 text-center clock" id="pageTimer">
                            
                        </div>
                        
                         <script>

                            // Set the date we're counting down to
                            var countDownDate = new Date(mies+" "+dzien+", "+rok+" "+godziny+":"+minuty+":"+sekundy).getTime();

                            // Update the count down every 1 second
                            var x = setInterval(function() {

                              // Get todays date and time
                              var now = new Date().getTime();

                              // Find the distance between now an the count down date
                              var distance = countDownDate - now;

                              // Time calculations for days, hours, minutes and seconds
                              var days = Math.floor(distance / (1000 * 60 * 60 * 24));
                              var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
                              var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
                              var seconds = Math.floor((distance % (1000 * 60)) / 1000);

                                days = (days < 10) ? "0"+days : days;
                                hours = (hours < 10) ? "0"+hours : hours;
                                minutes = (minutes < 10) ? "0"+minutes : minutes;
                                seconds = (seconds < 10) ? "0"+seconds: seconds; 
                                
                                
                                
                              // Display the result in the element with id="demo"
                              document.getElementById("pageTimer").innerHTML = days + ":" + hours + ":"
                              + minutes + ":" + seconds + " ";

                              // If the count down is finished, write some text 
                              if (distance < 0) {
                                clearInterval(x);
                                document.getElementById("pageTimer").innerHTML = "To już!";
                              }
                            }, 1000);

                             /*
                             countdown.setLabels(
                            ':0|:0|:0|:0|:0|:0|:0|:0|:0|:0|:0',
                            ' | |:|:|:|:|:|:|:|:|:',
                            '',
                            '',
                            '');
                             var timerId =
                                countdown(
                                data_przyszla,
                                    function(ts) {
                                      document.getElementById('pageTimer').innerHTML = ts.toHTML("span");
                                    },
                                countdown.MONTHS|countdown.DAYS|countdown.HOURS|countdown.MINUTES|countdown.SECONDS);

                            // later on this timer may be stopped
                            */
                            
                        </script>                       
                        
                        
                    </div> 
                    <div class="row text-center clock_label">
                        <div class="col">&nbsp;</div>
                        <div class="col">
                            dni
                        </div>
                        <div class="col">
                            godzin
                        </div>
                        <div class="col">
                            minut
                        </div>
                        <div class="col">
                            sekund
                        </div>
                        <div class="col">&nbsp;</div>
                    </div> 
                </div>
                <div class="col-md-6 text-center grey1">
                <div class="col-md-12 button_blue">Czas pozostały  <br /> do końca głosowania</div>
                </div>            
            </div>       

            <div class="row">
                <div class="col-md-12 spacing_top_medium">
                    <h1 class="grey1 text-left">Aktualny ranking</h1>
                </div>
            </div>        

             <div class="row">
                <div class="col-md-12">
                  <canvas id="myChart"></canvas>
                  <script>
                  var ctx = document.getElementById("myChart").getContext('2d');
                  var myChart = new Chart(ctx, {
                      type: 'horizontalBar',
                      data: {
                          labels: ["Firma 1", "Firma 2", "Firma 3", "Firma 4", "Firma 5"],
                          datasets: [{
                              label: 'liczba głosów',
                              data: [1400, 500, 500, 300, 600,],
                              backgroundColor: [
                                  'rgba(54, 162, 235, 0.2)',
                                  'rgba(54, 162, 235, 0.2)',
                                  'rgba(54, 162, 235, 0.2)',
                                  'rgba(54, 162, 235, 0.2)',
                                  'rgba(54, 162, 235, 0.2)',
                                  
                              ],
                              borderColor: [
                                  'rgba(54, 162, 235, 1)',
                                  'rgba(54, 162, 235, 1)',
                                  'rgba(54, 162, 235, 1)',
                                  'rgba(54, 162, 235, 1)',
                                  'rgba(54, 162, 235, 1)',
                                  
                              ],
                              borderWidth: 1
                          }]
                      },
                      options: {
                          scales: {
                              yAxes: [{
                                  ticks: {
                                      beginAtZero:true
                                  }
                              }]
                          }
                      }
                  });
                  </script>
                </div>
            </div>
            
            <div class="row">
                <div class="col-md-12 spacing_top_medium">
                    <h1 class="grey1 text-left">Zmiany w liczbie głosów (czołowa 5)</h1>
                </div>
            </div>        

             <div class="row">
                <div class="col-md-12">
                  <canvas id="myChart2"></canvas>
                  <script>
                  var ctx = document.getElementById("myChart2").getContext('2d');
                  var myChart = new Chart(ctx, {
                      type: 'line',
                      data: {
                          labels: ["13-mar", "14-mar", "15-mar", "16-mar", "17-mar", "18-mar", "19-mar", "20-mar", "21-mar", "22-mar", "23-mar", "24-mar"],
                          datasets: [
                          {
                              fill: 'false',
                              borderColor: 'rgb(85, 132, 188)',
                              borderWidth: 10,
                              label: 'Firma 1',
                              data: [
                                {x: 5,y: 50}, 
                                {x: 10,y: 40},
                                {x: 15,y: 20},
                                {x: 20,y: 10},
                                {x: 25,y: 50},
                                {x: 30,y: 70},
                                {x: 35,y: 92}, 
                                {x: 40,y: 95},
                                {x: 45,y: 99},
                                {x: 50,y: 104},
                                {x: 55,y: 105},
                                {x: 60,y: 108},
                                ],
                              borderWidth: 1
                          },
                          {
                              fill: 'false',
                              borderColor: 'rgb(255, 99, 132)',
                              label: 'Firma 2',
                              data: [
                                {x: 5,y: 140}, 
                                {x: 10,y: 130},
                                {x: 15,y: 105},
                                {x: 20,y: 98},
                                {x: 25,y: 32},
                                {x: 30,y: 12},
                                {x: 35,y: 48}, 
                                {x: 40,y: 45},
                                {x: 45,y: 34},
                                {x: 50,y: 49},
                                {x: 55,y: 75},
                                {x: 60,y: 83},
                                ],
                              borderWidth: 1
                          },

                          {
                              fill: 'false',
                              borderColor: 'rgb(154, 185, 91)',
                              label: 'Firma 3',
                              data: [
                                {x: 5,y: 0}, 
                                {x: 10,y: 3},
                                {x: 15,y: 17},
                                {x: 20,y: 23},
                                {x: 25,y: 24},
                                {x: 30,y: 26},
                                {x: 35,y: 29}, 
                                {x: 40,y: 43},
                                {x: 45,y: 44},
                                {x: 50,y: 48},
                                {x: 55,y: 52},
                                {x: 60,y: 62},
                                ],
                              borderWidth: 1
                          },
                          
                         {
                              fill: 'false',
                              borderColor: 'rgb(129, 100, 156)',
                              label: 'Firma 4',
                              data: [
                                {x: 5,y: 12}, 
                                {x: 10,y: 24},
                                {x: 15,y: 38},
                                {x: 20,y: 48},
                                {x: 25,y: 59},
                                {x: 30,y: 70},
                                {x: 35,y: 84}, 
                                {x: 40,y: 98},
                                {x: 45,y: 109},
                                {x: 50,y: 124},
                                {x: 55,y: 133},
                                {x: 60,y: 139},
                                ],
                              borderWidth: 1
                          },                          

                         {
                              fill: 'false',
                              borderColor: 'rgb(237, 206, 59)',
                              label: 'Firma 5',
                              data: [
                                {x: 5,y: 1}, 
                                {x: 10,y: 11},
                                {x: 15,y: 23},
                                {x: 20,y: 44},
                                {x: 25,y: 66},
                                {x: 30,y: 89},
                                {x: 35,y: 120}, 
                                {x: 40,y: 101},
                                {x: 45,y: 90},
                                {x: 50,y: 65},
                                {x: 55,y: 43},
                                {x: 60,y: 25},
                                ],
                              borderWidth: 1
                          },

                          


                      ]},
                      options: {
                          elements: {
                              line: {
                                  tension: 0, // disables bezier curves
                              }
                          },
                          scales: {
                              yAxes: [{
                                  ticks: {
                                      beginAtZero:true
                                  }
                              }]
                          }
                      }
                  });
                  </script>
                </div>
            </div>        

            <div class="row">
                <div class="col-md-12 spacing_top_medium spacing_bottom_medium"></div>
            </div>

            <div class="row">
                <div class="col-md-3 text-center">
                    <div class="button_blue">Jak głosować?</div>
                </div>
                <div class="col-md-3 text-center grey1">
                    <img src="img/krok1.jpg" />
                    <h1>Krok 1</h1>

                    <h2 class="arial">Wybierz kandydata</h2>

                </div>
                <div class="col-md-3 text-center grey1">

                    <img src="img/krok2.jpg" />
                    <h1>Krok 2</h1>

                    <h2 class="arial">Podaj numer telefonu</h2>
                    <h2 class="arial">Poczekaj na bezpłatny <br /> kod sms</h2>
                    <h2 class="arial">Po otrzymaniu kodu, <br /> wpisz go na stronie <br /> i zatwierdź</h2>

                </div>
                <div class="col-md-3 text-center grey1">
                    <img src="img/krok3.jpg" />
                    <h1>Krok 3</h1>  
                    <h2 class="arial">W tym momencie głos staje się ważny, a Twój numer telefonu uczestniczy w losowaniu nagród rzeczowych</h2>

                </div>            
            </div>        

        </div>
    </div>
    
    <div class="container-fluid back-grey">
        <div class="text-center breadcrumbs">
            <strong>Jesteś tutaj:</strong> <a href="#" class="bread">Podlaska marka</a> / <a href="#" class="bread">Aktualności</a> / Rzeczywiste wyniki głosowania
        </div>
    </div>
    
    <div class="container-fluid back-navy">
        <div class="container my_footer">
            <div class="row">
                <div class="stopka1 col-md-4">
                    Copyright by <strong>Podlaskie</strong> 2017 <br />
                    Wszelkie prawa zastrzeżone. <br />
                    tel. 85 66 54 452, 453, 454 <br />
                    <a href="#" class="footer_mail">pmr@wrotapodlasia.pl</a>
                </div>
                <div class="stopka2 col-md-4">
                    <img src="img/stopka_zubr.jpg" />
                </div>
                <div class="stopka3 col-md-4">
                    Obsługę przedsięwzięcia prowadzi <br />
                    Urząd Marszałkowski Województwa Podlaskiego <br />
                    <strong>Referat Promocji</strong><br />
                    ul. Kard. S. Wyszyńskiego 1, 15-888 Białystok
                </div>
            </div>    
        </div>
    </div>
    
    
</body>
</html>